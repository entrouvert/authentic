# Generated by Django 2.2.26 on 2022-09-22 09:52

import django
import django.db.models.deletion
from django.db import migrations, models

field_kwargs = {}


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2_auth_oidc', '0014_auto_20220920_1614'),
    ]

    operations = [
        migrations.AlterField(
            model_name='oidcclaimmapping',
            name='authenticator',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to='authenticators.BaseAuthenticator',
                **field_kwargs,
            ),
        ),
    ]
