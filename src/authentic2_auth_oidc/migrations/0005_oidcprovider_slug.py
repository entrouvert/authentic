# Generated by Django 1.11.12 on 2018-09-28 08:58

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2_auth_oidc', '0004_auto_20171017_1522'),
    ]

    operations = [
        migrations.AddField(
            model_name='oidcprovider',
            name='slug',
            field=models.SlugField(blank=True, max_length=256, null=True, unique=True, verbose_name='slug'),
        ),
    ]
