# Generated by Django 2.2.17 on 2020-11-02 10:42

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('authentic2_auth_oidc', '0007_auto_20200317_1732'),
    ]

    operations = [
        migrations.AlterField(
            model_name='oidcaccount',
            name='sub',
            field=models.CharField(max_length=256, verbose_name='sub'),
        ),
        migrations.AlterUniqueTogether(
            name='oidcaccount',
            unique_together={('provider', 'sub')},
        ),
    ]
