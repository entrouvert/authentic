# Generated by Django 2.2.27 on 2022-04-06 21:11

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2', '0039_add_unique_attribute_constraint'),
    ]

    operations = [
        migrations.AddField(
            model_name='userexternalid',
            name='external_guid',
            field=models.UUIDField(null=True, verbose_name='External GUID'),
        ),
        migrations.AlterField(
            model_name='userexternalid',
            name='external_id',
            field=models.CharField(max_length=256, null=True, verbose_name='external id'),
        ),
        migrations.AlterUniqueTogether(
            name='userexternalid',
            unique_together={('source', 'external_guid'), ('source', 'external_id')},
        ),
        migrations.AddConstraint(
            model_name='userexternalid',
            constraint=models.CheckConstraint(
                check=models.Q(
                    ('external_id__isnull', False), ('external_guid__isnull', False), _connector='OR'
                ),
                name='at_least_one_id',
            ),
        ),
    ]
