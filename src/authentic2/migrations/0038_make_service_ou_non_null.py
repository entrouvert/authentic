# Generated by Django 2.2.27 on 2022-04-07 15:05

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models

import authentic2.a2_rbac.utils
from authentic2.migrations import DropPartialIndexes


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2', '0037_auto_20220331_1513'),
    ]

    operations = [
        DropPartialIndexes(
            'Service', 'authentic2_service', 'authentic2_service_uniq_idx', ('ou_id',), ('slug',)
        ),
        migrations.AlterField(
            model_name='service',
            name='ou',
            field=models.ForeignKey(
                default=authentic2.a2_rbac.utils.get_default_ou_pk,
                on_delete=django.db.models.deletion.CASCADE,
                to=settings.RBAC_OU_MODEL,
                verbose_name='organizational unit',
            ),
        ),
    ]
