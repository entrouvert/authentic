# Generated by Django 1.11.12 on 2018-10-31 08:00

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('custom_user', '0016_auto_20180925_1107'),
        ('authentic2', '0022_attribute_scopes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userexternalid',
            name='source',
            field=models.CharField(max_length=256, verbose_name='source'),
        ),
    ]
