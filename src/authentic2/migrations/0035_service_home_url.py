# Generated by Django 2.2.23 on 2022-02-16 16:23

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2', '0034_attribute_required_on_login'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='home_url',
            field=models.URLField(blank=True, max_length=256, null=True, verbose_name='Home URL'),
        ),
    ]
