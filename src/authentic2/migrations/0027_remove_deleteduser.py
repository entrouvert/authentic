# Generated by Django 1.11.29 on 2020-04-21 14:09

from django.db import migrations, models


def fill_deleted(apps, schema_editor):
    DeletedUser = apps.get_model('authentic2', 'DeletedUser')
    User = apps.get_model('custom_user', 'User')
    User.objects.update(
        deleted=models.Subquery(
            DeletedUser.objects.filter(user_id=models.OuterRef('id')).values_list('creation')[:1]
        )
    )


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2', '0026_token'),
        ('custom_user', '0019_add_user_deleted'),
    ]

    run_before = [
        ('custom_user', '0026_remove_user_deleted'),
    ]

    operations = [
        migrations.RunPython(fill_deleted, migrations.RunPython.noop),
        migrations.RemoveField(
            model_name='deleteduser',
            name='user',
        ),
        migrations.DeleteModel(
            name='DeletedUser',
        ),
    ]
