# Generated by Django 2.2.26 on 2022-09-27 10:55

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('custom_user', '0031_profile_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deleteduser',
            name='old_uuid',
            field=models.TextField(blank=True, db_index=True, null=True, verbose_name='Old UUID'),
        ),
    ]
