# Generated by Django 2.2.26 on 2022-10-06 19:58

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('custom_user', '0032_auto_20220919_1230'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='keepalive',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Keepalive timestamp'),
        ),
    ]
