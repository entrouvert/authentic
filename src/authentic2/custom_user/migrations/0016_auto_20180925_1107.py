# Generated by Django 1.11.12 on 2018-09-25 09:07

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('custom_user', '0015_auto_20170707_1653'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='first_name',
            field=models.CharField(blank=True, max_length=128, verbose_name='first name'),
        ),
        migrations.AlterField(
            model_name='user',
            name='last_name',
            field=models.CharField(blank=True, max_length=128, verbose_name='last name'),
        ),
    ]
