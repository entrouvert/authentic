# Generated by Django 3.2.19 on 2024-06-03 13:02

import django.db.models.expressions
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('custom_user', '0036_remove_user_constraint_at_least_one_identifier'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='user',
            index=models.Index(
                django.db.models.expressions.F('last_name'),
                django.db.models.expressions.F('first_name'),
                name='custom_user_user_names',
            ),
        ),
    ]
