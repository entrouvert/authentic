# Generated by Django 1.11.29 on 2020-04-21 13:38

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('custom_user', '0018_user_last_account_deletion_alert'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='deleted',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Deletion date'),
        ),
    ]
