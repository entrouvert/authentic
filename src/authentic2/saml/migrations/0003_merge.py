from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('saml', '0002_auto_20150320_1245'),
        ('saml', '0002_ease_federation_migration'),
    ]

    operations = []
