# Generated by Django 3.2.25 on 2024-10-14 10:42

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('saml', '0020_libertysession_saml_libert_provide_39bb6c_idx'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='libertyserviceprovider',
            name='users_can_manage_federations',
        ),
    ]
