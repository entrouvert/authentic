# Generated by Django 3.2.18 on 2023-09-27 13:19

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('authenticators', '0017_auto_20230927_1517'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='addroleaction',
            name='attribute_name',
        ),
        migrations.RemoveField(
            model_name='addroleaction',
            name='attribute_value',
        ),
    ]
