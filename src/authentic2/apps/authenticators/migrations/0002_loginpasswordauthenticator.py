# Generated by Django 2.2.28 on 2022-04-13 12:56

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('authenticators', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LoginPasswordAuthenticator',
            fields=[
                (
                    'baseauthenticator_ptr',
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to='authenticators.BaseAuthenticator',
                    ),
                ),
                (
                    'remember_me',
                    models.PositiveIntegerField(
                        blank=True,
                        help_text='Session duration as seconds when using the remember me checkbox. Leave blank to hide the checkbox.',
                        null=True,
                        verbose_name='Remember me duration',
                    ),
                ),
                (
                    'include_ou_selector',
                    models.BooleanField(default=False, verbose_name='Include OU selector in login form'),
                ),
            ],
            options={
                'verbose_name': 'Password',
            },
            bases=('authenticators.baseauthenticator',),
        ),
    ]
