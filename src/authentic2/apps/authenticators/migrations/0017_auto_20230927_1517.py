# Generated by Django 3.2.18 on 2023-09-27 13:17

from django.db import migrations


def migrate_add_role_condition(apps, schema_editor):
    AddRoleAction = apps.get_model('authenticators', 'AddRoleAction')
    for action in AddRoleAction.objects.all():
        if action.condition:
            # ignore actions with defined condition
            continue
        action.condition = 'attributes.%s in "%s"' % (
            action.attribute_name,
            action.attribute_value,
        )
        action.save()


class Migration(migrations.Migration):
    dependencies = [
        ('authenticators', '0016_alter_addroleaction_condition'),
    ]

    operations = [
        migrations.RunPython(migrate_add_role_condition, reverse_code=migrations.RunPython.noop),
    ]
