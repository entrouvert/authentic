# Authentic2 © Entr'ouvert

from django.urls.converters import SlugConverter, register_converter


class ObjectUUID:
    regex = r'[0-9a-f]{32}'

    def to_python(self, value):
        return value.replace('-', '')

    def to_url(self, value):
        return str(value)


def register_converters():
    register_converter(ObjectUUID, 'a2_uuid')
    # legacy, some instances have non UUID like string as User.uuid
    register_converter(SlugConverter, 'user_uuid')
