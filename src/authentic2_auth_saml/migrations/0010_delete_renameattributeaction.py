# Generated by Django 2.2.26 on 2022-09-14 12:46

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2_auth_saml', '0009_statically_rename_attributes'),
    ]

    operations = [
        migrations.DeleteModel(
            name='RenameAttributeAction',
        ),
    ]
