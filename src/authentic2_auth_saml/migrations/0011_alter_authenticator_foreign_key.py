# Generated by Django 2.2.26 on 2022-09-20 14:48

import django.db.models.deletion
from django.db import migrations, models

addroleaction_field_kwargs = {}
samlattributelookup_field_kwargs = {}
setattributeaction_field_kwargs = {}


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2_auth_saml', '0010_delete_renameattributeaction'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addroleaction',
            name='authenticator',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to='authenticators.BaseAuthenticator',
                **addroleaction_field_kwargs,
            ),
        ),
        migrations.AlterField(
            model_name='samlattributelookup',
            name='authenticator',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to='authenticators.BaseAuthenticator',
                **samlattributelookup_field_kwargs,
            ),
        ),
        migrations.AlterField(
            model_name='setattributeaction',
            name='authenticator',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to='authenticators.BaseAuthenticator',
                **setattributeaction_field_kwargs,
            ),
        ),
    ]
