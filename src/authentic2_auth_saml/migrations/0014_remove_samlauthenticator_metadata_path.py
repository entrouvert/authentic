# Generated by Django 2.2.26 on 2022-10-26 10:09

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2_auth_saml', '0013_metadata_file_to_db'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='samlauthenticator',
            name='metadata_path',
        ),
    ]
