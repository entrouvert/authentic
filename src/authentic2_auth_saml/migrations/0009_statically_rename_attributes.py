# Generated by Django 2.2.26 on 2022-08-24 14:55

import logging

from django.db import migrations

logger = logging.getLogger('authentic2.auth_saml')


def rename_attributes(apps, schema_editor):
    SAMLAuthenticator = apps.get_model('authentic2_auth_saml', 'SAMLAuthenticator')

    for authenticator in SAMLAuthenticator.objects.all():
        rename_map = {x.to_name: x.from_name for x in authenticator.rename_attribute_actions.all()}

        for action in authenticator.set_attribute_actions.all():
            action.saml_attribute = rename_map.get(action.saml_attribute, action.saml_attribute)
            action.save()

        for lookup in authenticator.attribute_lookups.all():
            lookup.saml_attribute = rename_map.get(lookup.saml_attribute, lookup.saml_attribute)
            lookup.save()


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2_auth_saml', '0008_auto_20220913_1105'),
    ]

    operations = [
        migrations.RunPython(rename_attributes, reverse_code=migrations.RunPython.noop),
    ]
