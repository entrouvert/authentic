from django.apps import AppConfig


class DjangoRBACConfig(AppConfig):
    name = 'django_rbac'
    verbose_name = 'RBAC engine for Django'
