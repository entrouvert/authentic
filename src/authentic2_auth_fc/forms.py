# authentic2-auth-fc - authentic2 authentication for FranceConnect
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.translation import gettext_lazy as _

from . import app_settings
from .models import SCOPE_CHOICES, FcAuthenticator


class FcAuthenticatorForm(forms.ModelForm):
    scopes = forms.MultipleChoiceField(
        label=_('Scopes'),
        choices=SCOPE_CHOICES,
        widget=forms.CheckboxSelectMultiple(),
        help_text=_('These scopes will be requested in addition to openid'),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if app_settings.display_common_scopes_only:
            self.fields['scopes'].choices = [
                ('family_name', _('family name (family_name)')),
                ('given_name', _('given name (given_name)')),
                ('birthdate', _('birthdate (birthdate)')),
                ('birthplace', _('birthplace (birthplace)')),
                ('birthcountry', _('birthcountry (birthcountry)')),
                ('gender', _('gender (gender)')),
                ('preferred_username', _('usual family name (preferred_username)')),
                ('email', _('email (email)')),
            ]

    class Meta:
        model = FcAuthenticator
        exclude = ('name', 'slug', 'ou', 'button_description', 'button_label', 'jwkset_json')
